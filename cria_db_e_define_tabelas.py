from sqlalchemy import(create_engine, MetaData, Column,
                    Table, Integer, String,DateTime)
from datetime import datetime

engine = create_engine('sqlite:///teste.db', echo = False)


metadata = MetaData(bind = engine)

SubstratoTAB = Table('Substrato', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('Material',String(40)),
                    Column('Protocolo',String(40)),
                    Column('Fabricante',String(40)),
                    Column('Unidade',String(40))
                    )
                    

EletrodoInferiorTAB = Table('EletrodoInferior', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('Camada', String(40)),
                    Column('Material',String(40)),
                    Column('Metodo',String(40)),
                    Column('Lote',String(40)),
                    Column('Unidade',String(40)),
                    Column('Resistencia',String(40))
                    )
                    

MatrizTAB = Table('Matriz', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('PropCondutor',String(40)),
                    Column('PropPolimetro',String(40)),
                    Column('Aditivo',String(40)),
                    Column('Solvente',String(40)),
                    Column('EstabilizanteQuimico',String(40))
                    )
                    

MaterialCamadaAtivaTAB = Table('MaterialCamadaAtiva', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('Silicato',String(40)),
                    Column('LoteMaterial',String(40)),
                    Column('Empresa',String(40)),
                    Column('PropSilicato',String(40)),
                    Column('Matriz',String(40)),
                    Column('PropMatriz',String(40)),
                    Column('Viscosidade',String(40)),
                    Column('MatrizAditivo',String(40))
                    )
DeposicaoCamadaAtivaTAB = Table('DeposicaoCamadaAtiva', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('LoteCamada',String(40)),
                    Column('Metodo',String(40)),
                    Column('Lineatura',String(40))
                    )
EletrodoSuperiorTAB = Table('EletrodoSuperior', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40)),
                    Column('Camada',String(40)),
                    Column('Material',String(40)),
                    Column('Metodo',String(40)),
                    Column('Lote',String(40)),
                    Column('Unidade',String(40)),
                    Column('Resistencia',String(40))
                    )

TintaAzulTAB = Table('TintaAzul', metadata,
                    Column('id',String(40),primary_key=True,autoincrement=False),
                    Column('Operador', String(40))
                    )

IdTAB = Table('IDs',metadata,
                Column('id',String(40),primary_key=True,autoincrement=False),
                Column('Operador', String(40)),
                Column('data',String(26),default=datetime.now()))

                    

metadata.create_all()