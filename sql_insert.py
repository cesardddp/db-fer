from datetime import datetime
from  cria_db_e_define_tabelas import (SubstratoTAB,
                        EletrodoInferiorTAB,
                        MatrizTAB,
                        MaterialCamadaAtivaTAB,
                        DeposicaoCamadaAtivaTAB,
                        EletrodoSuperiorTAB,
                        TintaAzulTAB,
                        IdTAB,
                        engine)



conn = engine.connect()


def conexecutor(func):
    global conn
    def decorador(uid,args):
        a = func(uid,**args)
        conn.execute(a) 
    return decorador

@conexecutor
def insere_Substrato (uid = None,operador=None,material=None,protocolo=None,fabricante=None,unidade=None):
    novo_substrato = SubstratoTAB.insert().values(
                        id = uid,
                        Operador = operador,
                        Material = material,
                        Protocolo = protocolo,
                        Fabricante = fabricante,
                        Unidade = unidade
                        )
    return novo_substrato

@conexecutor                    
def insere_EletrodoInferior (uid = None,operador=None,camada=None,material=None,metodo=None,lote=None,unidade=None,resistencia=None):
    novo_EletrodoInferior = EletrodoInferiorTAB.insert().values(
                        id = uid,
                        Operador = operador,
                        Camada = camada,
                        Material = material,
                        Metodo = metodo,
                        Lote = lote,
                        Unidade = unidade,
                        Resistencia = resistencia
                        )
    return novo_EletrodoInferior

@conexecutor                    
def insere_Matriz (uid = None,operador = None,propCondutor = None,propPolimetro = None,aditivo = None,solvente = None,estabilizanteQuimico = None):
    novo_Matriz = MatrizTAB.insert().values(
                        id = uid,
                        Operador = operador,
                        PropCondutor = propCondutor,
                        PropPolimetro = propPolimetro,
                        Aditivo = aditivo,
                        Solvente = solvente,
                        EstabilizanteQuimico = estabilizanteQuimico
    )                           
    return novo_Matriz

@conexecutor                    
def insere_MaterialCamadaAtiva(uid = None,operador = None,silicato = None,loteMaterial = None,empresa = None,propSilicato = None,matriz = None,
propMatriz = None,viscosidade = None,matrizAditivo = None):
    novo_MaterialCamadaAtiva = MaterialCamadaAtivaTAB.insert().values(
                        id = uid,
                        Operador = operador ,
                        Silicato = silicato,
                        LoteMaterial = loteMaterial,
                        Empresa = empresa,
                        PropSilicato = propSilicato,
                        Matriz = matriz,
                        PropMatriz = propMatriz,
                        Viscosidade = viscosidade,
                        MatrizAditivo = matrizAditivo
                    )
    return novo_MaterialCamadaAtiva

@conexecutor
def insere_DeposicaoCamadaAtiva (uid = None,operador = None,loteCamada = None,metodo = None,lineatura = None):
    novo_DeposicaoCamadaAtiva = DeposicaoCamadaAtivaTAB.insert().values(
                        id = uid,
                        Operador = operador,
                        LoteCamada = loteCamada,
                        Metodo = metodo,
                        Lineatura = lineatura
                        )
    return novo_DeposicaoCamadaAtiva

@conexecutor
def insere_EletrodoSuperior (uid = None,operador = None,camada = None,material = None,metodo = None,lote = None,unidade = None,resistencia = None):
    novo_EletrodoSuperior = EletrodoSuperiorTAB.insert().values(
                        id = uid,
                        Operador = operador,
                        Camada = camada,
                        Material = material,
                        Metodo = metodo,
                        Lote = lote,
                        Unidade = unidade,
                        Resistencia = resistencia
                        )
    return novo_EletrodoSuperior

@conexecutor
def insere_TintaAzul (uid = None,operador = None):
    novo_TintaAzul = TintaAzulTAB.insert().values(
                        id = uid,
                        Operador = operador
                        )
    return novo_TintaAzul

@conexecutor
def insere_IDs(uid=None):
    return IdTAB.insert().values(
        id=uid,
        data=datetime.now()
    )
        

#     )
# foo()    
# conn.execute([novo_Substrato,novo_DeposicaoCamadaAtiva,novo_EletrodoInferior,novo_EletrodoSuperior,novo_MaterialCamadaAtiva,novo_Matriz,novo_TintaAzul])

#executar varias coisa de uma vez
# conn.execute(novo_.insert(),[
#     {'Operador':'bia ='Material':'rocha ='Protocolo': 223,'Fabricante':'lenovo ='Unidade':'rio preto'},
#     {'Operador':'fer ='Material':'arocha ='Protocolo': 233,'Fabricante':'sanguesung ='Unidade':'rio de dezembro'},
#     {'Operador':'laura ='Material':'dianmente ='Protocolo': 333,'Fabricante':'beteza ='Unidade':'rio que corre'}
# ])