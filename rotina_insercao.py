from sql_insert import (insere_Substrato,
        insere_EletrodoInferior,
        insere_Matriz,
        insere_MaterialCamadaAtiva,
        insere_DeposicaoCamadaAtiva,
        insere_EletrodoSuperior,
        insere_TintaAzul,
        insere_IDs)
from uuid import uuid4

def nova_tabela_preta(dados):
    """
    :param dados: é uma lista de dicionarios
    cada dicionario é referente a uma guia:
    substrato :     {'operador':'valor deoperador',
                     'material' : 'valor de material',
                     'protocolo':'valor de protocolo', etc}
    aparencia de dados:
    dados = [
            {'operador':'valor de operador','material': 'valor de material','protocolo':'valor de protocolo', etc},
            {'operador':'valor de operador','camada':'valor de camada','material'='valor de material','metodo'='valor de metodo',etc}
            {'operador':'valor de operador','propCondutor': 'valor de propCondutor','propPolimetro' : 'valor de propPolimetro',etc}
            ]
    """
    uid = str(uuid4())
    
    funções = [
        insere_Substrato,
        insere_EletrodoInferior,
        insere_Matriz,
        insere_MaterialCamadaAtiva,
        insere_DeposicaoCamadaAtiva,
        insere_EletrodoSuperior,
        insere_TintaAzul,
    ]
    for função,dado in zip(funcões,dados):
        funcão(uid,dado)

#insere_IDs(uid,{})

####################### para teste
# dados = [
# {'operador':'nomes','material':'foo','protocolo':'foo','fabricante':'foo','unidade':'foo'},
# {'operador':'foo','camada':'foo','material':'foo','metodo':'foo','lote':'foo','unidade':'foo','resistencia':'foo'},
# {'operador':'foo','propCondutor':'foo','propPolimetro':'foo','aditivo':'foo','solvente':'foo','estabilizanteQuimico':'foo'},
# {'operador':'nomes','silicato':'foo','loteMaterial':'foo','empresa':'foo','propSilicato':'foo','matriz':'foo','propMatriz':'foo',
# 'viscosidade':'foo','matrizAditivo':'foo'},
# {'operador':'nomes','loteCamada':'foo','metodo':'foo','lineatura':'foo'},
# {'operador':'nomes','camada':'foo','material':'foo','metodo':'foo','lote':'foo','unidade':'foo','resistencia':'foo'},
# {'operador':'nomes'}
# ]
# nova_tabela_preta(dados)

    
        

